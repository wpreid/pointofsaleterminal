#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Feature_IM @WIP
Feature: Inventory Management
  
  This feature is all about managing the store catalog and what is says about 
  the products we sell, their prices, how many items we have in stock, and 
  where in the store I can find those items.   
  
  The scenarious below allow us to do the following when managing inventory: 
  - Add new products to the store catalog with initial values for: 
       (upc, description, location, unit price, and quantity of items on hand) 
  - Retrieve a list of products currently in the store catalog 
  - Update all product attributes associated with a given upc 
  - Update quantities of existing items when we receive shipments
  - Remove an item from the store catalog based on upc
     
  
   
  PREVIOUS FEATURE DESCRIPTION 
  As a Manager, 
  I want to add new products to the catalog of items the store sells,
   and update quantities of existing items when we receive shipments,
  so that I stock items our customers want.  
  
  Acceptance Criteria: 
  
  I can enter a new products upc, description, location, unit price, and quantity of items on hand.
  
  For existing products, I can easily enter additional quantities of items and the system will update the total.

  Background: 
    Given I have a new store catalog

  @IM_AddProduct
  Scenario Outline: Add new product to the store catalog
    
    As a Manager, 
    I want to add new products to the catalog of items the store sells,
    so that I stock items our customers want.
    
    UI Screen Example: https://dz2cdn1.dzone.com/storage/temp/14004241-mockup.png

    Given I have a new store catalog
    And a product to add with upc "<upc>"
    * description "<product description>"
    * unit price of <unit price>
    * quantity on hand of <quantity on hand>
    * located at "<store location>"
    When I add the product to the store catalog
    Then it should be "<result>"

    Examples: 
      | Scenario            | upc     | product description | unit price | quantity on hand | store location | result |
      | Happy Path          |     100 | Water Bottle        | $2.00      |              102 | fridge         | OK     |
      | Bad UPC             | '@#$%!' | Coffee Grinder      | $15.00     |                1 | entry display  | ERROR  |
      | Missing Description |     900 |                     | $2.00      |              102 | fridge         | ERROR  |
      | Negative Quantity   |     900 | Water Bottle        | $2.00      |               -1 | fridge         | ERROR  |
      | Zero Unit Price     |     900 | Water Bottle        | $0.00      |              102 | fridge         | ERROR  |
      | Negative Unit Price |     900 | Water Bottle        | $-1.00     |              102 | fridge         | ERROR  |
      | Missing Location    |     900 | Water Bottle        | $2.00      |              102 |                | ERROR  |

  @IM_Duplicates
  Scenario: Duplicate items not allowed in store catalog (Option A)
    
    As a Manager, 
    I want to make sure duplicate products can not be added to the store catalog, 
    so that product mistakes are not made

    Given the store catalog contains:
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |               85 | fridge         |
    And a product to add with upc "100"
    * description "Water Bottle"
    * unit price of $1.00
    * quantity on hand of 85
    * located at "fridge"
    When I add the product to the store catalog
    Then it should be "ERROR"

  @IM_Duplicates
  Scenario: Two different products may not have the same UPC code (Option B)
    
    As a Manager, 
    I want to make sure duplicate products can not be added to the store catalog, 
    so that product mistakes are not made

    Given the following store catalog list:
      | upc | product description |
      | 100 | Water Bottle        |
    And a product to add with upc "100"
    * description "Lemonade"
    * unit price of $1.00
    * quantity on hand of 85
    * located at "fridge"
    When I add the product to the store catalog
    Then it should be "ERROR"

  @IM_Duplicates
  Scenario: Two different products may not have the same UPC code (Option C)
    
    As a Manager, 
    I want to make sure duplicate products can not be added to the store catalog, 
    so that product mistakes are not made

    Given the following store catalog list:
      | upc | product description |
      | 100 | Water Bottle        |
    When I add to the store catalog "Lemonade" with upc "100"
    Then it should be "ERROR"

  @IM_Duplicates
  Scenario: Two different products may not have the same UPC code (Option D)
    
    As a Manager, 
    I want to make sure duplicate products can not be added to the store catalog, 
    so that product mistakes are not made

    Given the store catalog contains "Water Bottle" with upc "100"
    When I add to the store catalog "Lemonade" with upc "100"
    Then it should be "ERROR"

  @IM_Update_Desc
  Scenario: Update product descriptions
    
    As a Manager, 
    I want to be able to update product descriptions in the store catalog, 
    so that the store catelog is always correct

    Given the store catalog contains:
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |               85 | fridge         |
    When I update product with upc "100" to have the description "Gatorade"
    Then product description for upc "100" should be "Gatorade"

  @IM_Update_Quantities
  Scenario: Update product quantities
    
    As a Manager, 
    I want to be able to update quantity on hand for products in the store catalog, 
    so that the store catelog accurately reflects how many items we have to sell

    Given the store catalog contains:
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |                5 | fridge         |
    When we add 50 items of "100" to the catalog
    Then there should be 55 items of "100" in stock

  @tag3
  Scenario Outline: Look up a product with UPC code
    
    As a Store Manager 
    I want to be able to find products in inventory 
    So that I know how many we have on hand

    Given the store catalog contains:
      | upc | product description   | unit price | quantity on hand | store location |
      | 100 | Water Bottle          |       1.00 |               85 | fridge         |
      | 110 | Diet Coke 12oz Bottle |       1.50 |              195 | fridge         |
      | 112 | Coke 12oz Bottle      |       1.50 |              123 | fridge         |
      | 114 | Chocolate Cup Cakes   |       2.25 |               16 | Shelf C        |
    When I lookup the product with UPC code "<upc>"
    Then the result should be "<outcome>"

    Examples: 
      | scenario    | upc            | outcome           |
      | found       |            100 | product found     |
      | not found   |            999 | product not found |
      | invalid UPC | XXX            | invalid UPC       |
      | invalid UPC | 114s           | invalid UPC       |
      | found       |            112 | product found     |
      | invalid UPC |                | invalid UPC       |
      | Long UPC    | 11111111111111 | product not found |
