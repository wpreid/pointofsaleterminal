package post.domain;

import java.util.ArrayList;
import java.util.List;

public class Sale {
	private List<SaleLineItem> items = new ArrayList<SaleLineItem>();

	public void enterItem(SaleLineItem item) {
		items.add(item);
	}

	public double getTotal() {
		double total = 0;
		for (SaleLineItem item : items) {
			total += item.extendedPrice();	
		}
		return total;
	}

	public void createLineItem(int quantity, ProductSpecification product) {
		SaleLineItem item = new SaleLineItem(product, quantity);
		enterItem(item);
	}
}
