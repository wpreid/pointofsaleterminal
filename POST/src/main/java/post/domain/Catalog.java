package post.domain;

import java.util.HashMap;
import java.util.Map; 

public class Catalog {
	public Map<UPC, ProductSpecification> upcProductMap;

	public Catalog() {
		upcProductMap = new HashMap<UPC, ProductSpecification>();
	}

	public ProductSpecification lookup(String upcCode) {
		return upcProductMap.get(new UPC(upcCode));
	}

	public void addNewProduct(Post post, String upc, String desc, double price, int quantityOnHand, String location) {
		addNewProduct(upc, desc, price, quantityOnHand, location);
	}

	public void addNewProduct(String upcCode, String desc, double price, int quantityOnHand, String location) {
		UPC upc = new UPC(upcCode);
		if (upcProductMap.containsKey(upc)) {
			throw new IllegalArgumentException("duplicate UPC");
		}
		upcProductMap.put(upc, new ProductSpecification(upc, desc, price, quantityOnHand, location));
	}

	public void updateQuantity(String upcCode, int additionalQuantity) {
		ProductSpecification product = lookup(upcCode);
		product.updateQuantity(additionalQuantity);
	}
	
	public void updateDescription(String upcCode, String newDescription) {
		ProductSpecification product = lookup(upcCode);
		product.updateDescription(newDescription);
	}
}