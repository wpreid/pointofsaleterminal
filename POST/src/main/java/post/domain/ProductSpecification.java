package post.domain;

public class ProductSpecification {
	private UPC upc;
	private String description;
	private double price;
	private int quantityOnHand;
	private String location;

	public ProductSpecification(String upc, String description, double price, int quantity, String location) {
		this(new UPC(upc), description, price, quantity, location);
	}
	
	public ProductSpecification(UPC upc, String description, double price, int quantityOnHand, String location) {
		this.upc = upc;
		this.description = description == null ? null: description.trim();
		this.price = price;
		this.quantityOnHand = quantityOnHand;
		this.location = location;
		
		if (this.upc == null)
			throw new IllegalArgumentException("missing upc");
		
		if (this.description == null || this.description.equals(""))
			throw new IllegalArgumentException("missing description");
		
		if (this.price <= 0)
			throw new IllegalArgumentException("invalid unit price");
		
		if (this.quantityOnHand < 0)
			throw new IllegalArgumentException("invalid quantity");
		
		if (this.location == null || "".equals(location))
			throw new IllegalArgumentException("missing location");
	}

	public double getPrice() {
		return price;
	}

	public UPC getUpc() {
		return upc;
	}

	public String getUpcCode() {
		return upc.getUpcCode();
	}

	public String getDescription() {
		return description;
	}

	public int getQuantity() {
		return quantityOnHand;
	}

	public String getLocation() {
		return location;
	}

	public void updateQuantity(int additionalQuantity) {
		if (quantityOnHand + additionalQuantity < 0)
			throw new IllegalArgumentException("can't reduce inventory below zero");
		quantityOnHand += additionalQuantity;
	}
	
	public void updateDescription(String newDescription) {
		if (newDescription.length() <= 0 )
			throw new IllegalArgumentException("product description must not be empty");
		description = newDescription;
	}
}
