package post.domain;

public class SaleLineItem {
	private ProductSpecification product;
	private int quantity;

	public SaleLineItem(ProductSpecification product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}
	
	public ProductSpecification getProduct() {
		return product;
	}

	public double extendedPrice() {
		return getProduct().getPrice() * getQuantity();
	}
}
