package post.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

//import post.domain.Post;
//import post.domain.ProductSpecification;

public class AddInventoryTest {
	private Post post = new Post();

	@Before
	public void setup() {

	}

	@Test
	public void addNewProductShouldAddProductToCatalog() {
		post.addNewProduct("100", "Water Bottle", 2.00, 102, "fridge");
		ProductSpecification product = post.lookupItem("100");

		assertNotNull(product);
		assertEquals("100", product.getUpcCode());
		assertEquals("Water Bottle", product.getDescription());
		assertEquals(2.00, product.getPrice(), 0.005);
		assertEquals(102, product.getQuantity());
		assertEquals("fridge", product.getLocation());
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowDuplicates() {
		post.addNewProduct("100", "Water Bottle", 2.00, 102, "fridge");
		post.addNewProduct("100", "Biscotti", 1.50, 100, "bakery case");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowBadUPC() {
		post.addNewProduct(null, "Coffee Grinder", 15.00, 1, "entry display");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowEmptyUPC() {
		post.addNewProduct("", "Water Bottle", 2.00, 102, "fridge");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowInvalidUPC() {
		post.addNewProduct("@#$%!", "Water Bottle", 2.00, 102, "fridge");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowNullDescription() {
		post.addNewProduct("900", null, 1.00, 10, "location");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowEmptyDescription() {
		post.addNewProduct("900", " ", 1.00, 10, "location");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowNegativeQuantity() {
		post.addNewProduct("900", "Santa Mugs", 1.00, -1, "location");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowZeroUnitPrice() {
		post.addNewProduct("900", "Coffee Grinder", 0.00, 12, "location");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowNegativeUnitPrice() {
		post.addNewProduct("900", "Coffee Beans", -10.00, 24, "product shelf");
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowNullLocation() {
		post.addNewProduct("900", "Travel Mugs", 14.00, 16, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNewProductShouldNotAllowEmptyLocation() {
		post.addNewProduct("900", "Travel Mugs", 14.00, 16, "");
	}

	public void unknownProductShouldReturnNull() {
		assertNull(post.lookupItem("999"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void productLookupShouldNotAllowInalidUPC() {
		post.lookupItem("XXX");
	}
}
