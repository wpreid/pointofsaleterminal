package post.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

//import post.domain.Post;

public class PostTest {
	private Post post = new Post();

	@Before
	public void setup() {
		post.addNewProduct("1", "Bubble Gum Tape", 1.00, 0, "checkout");
		post.addNewProduct("2", "Milk, 1 gallon", 5.00, 0, "fridge");
	}

	@Test
	public void totalShouldReturnSumTotalOfAllItems() {
		post.enterItem("1", 2);
		post.enterItem("2", 1);

		assertEquals(7.00, post.getTotal(), 0.001);
	}
}
